-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2023 at 11:55 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insertfile`
--

-- --------------------------------------------------------

--
-- Table structure for table `multipleimageupload`
--

CREATE TABLE `multipleimageupload` (
  `imageName` varchar(200) DEFAULT NULL,
  `imageId` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `multipleimageupload`
--

INSERT INTO `multipleimageupload` (`imageName`, `imageId`) VALUES
('alice.jpg', 16),
('android_pro.jpg', 15),
('australia_flag.png', 14),
('awaken_the_giant.jpg', 13),
('boy1.jpg', 12),
('boy2.png', 11),
('sister.jpg', 7),
('smiley.jpg', 6),
('the_diary.jpg', 0),
('twitter.jpg', 5),
('whatsapp.png', 4),
('wishes.jpg', 10),
('wuthering-1679653554.jpg', 17),
('wuthering.jpg', 3),
('zoom_in.jpg', 2),
('zoom_out-1679646708.jpg', 8),
('zoom_out-1679646897.jpg', 9),
('zoom_out.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pdfs_upload`
--

CREATE TABLE `pdfs_upload` (
  `pid` int(30) NOT NULL,
  `pdf_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pdfs_upload`
--

INSERT INTO `pdfs_upload` (`pid`, `pdf_name`) VALUES
(1, 'woman-in-white.pdf'),
(12, 'Love Never Fails By Kenneth E. Hagin ( PDFDrive ).pdf'),
(13, 'midnight_library.pdf'),
(14, 'old_man.pdf'),
(15, 'Gullivers-Travels-Jonathan-Swift-www.indianpdf.com_-Download-eBook-Novel-Free-Online.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `videos_upload`
--

CREATE TABLE `videos_upload` (
  `vid` int(30) NOT NULL,
  `url` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `videos_upload`
--

INSERT INTO `videos_upload` (`vid`, `url`) VALUES
(1, 'https://www.youtube.com/watch?v=_F8WS0FhyDs'),
(2, 'https://www.youtube.com/watch?v=idnnxV5OyII'),
(3, 'https://www.youtube.com/watch?v=yL1mZCxCWEc'),
(4, '\nhttps://www.youtube.com/watch?v=MNdL6AMT8qo'),
(5, 'https://www.youtube.com/watch?v=HqVoqEHl1-4'),
(7, 'https://www.youtube.com/watch?v=TckGcxwknYU'),
(8, 'https://www.youtube.com/watch?v=k6OPqsueZYk');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `multipleimageupload`
--
ALTER TABLE `multipleimageupload`
  ADD PRIMARY KEY (`imageId`),
  ADD UNIQUE KEY `pdf` (`imageName`);

--
-- Indexes for table `pdfs_upload`
--
ALTER TABLE `pdfs_upload`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `videos_upload`
--
ALTER TABLE `videos_upload`
  ADD PRIMARY KEY (`vid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pdfs_upload`
--
ALTER TABLE `pdfs_upload`
  MODIFY `pid` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `videos_upload`
--
ALTER TABLE `videos_upload`
  MODIFY `vid` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
